# 信息系统项目管理师之论文
相比中级信息系统项目管理师，高级主要增加了论文的考评。
论文满分75分，及格界限45分。

* 最初觉得论文肯能是最难的一项考试，但进过了解后发现论文其实是有一定方法的。
* 论文是在下午的第二节考试，时长为两小时，要求编写2500字左右的论文。
* 论文一定要提前准备，提前练习；否者有的同学可能书写不完或有的字不会写。
* 从19年开始没有明确要求写摘要了，如实际考试时没有明确要求可以不写，以免浪费时间，但是准备沦为时依然需要准备摘要，以免翻车。

## 一、论文范围
 每次考试都会有两个论文题目，只需选择一个题目进行写作即可。
 分析以往的论文考试题目，发现每次的论文都会包含**十大管理**的题目。
 所以我们备考论文时只需要准备十大管理相关的内容即可。
 目前所有的培训机构也都是这么让学员备考的。

论文整体来说，可以分为过程类和控制类，我们首先应该搞懂的是过程类。
按项目大小可分为大项目论文和一般项目论文。

**十大管理及过程包括：**  

|十大管理\五大过程组			|启动过程组		|计划过程组				|执行过程组					|控制过程组		|收尾过程组|
|:--|:--|:--|:--|:--|:--|
|1. 整体管理			|①制定项目章程	|②编制项目管理计划	|③指导和管理项目执行	|④监督和控制项目工作<br>⑤整体变更控制 |⑥项目收尾|
|2. 范围管理			|/	|①编制范围管理计划<br>②收集需求<br>③范围定义<br>④建立WBS|/|⑤范围确认<br>⑥范围控制|/	|
|3. 进度管理			|/ 	|①编制进度管理计划<br>②活动定义<br>③活动排序<br>④资源估算<br>⑤历时估算<br>⑥制定进度计划| / |⑦进度控制|/ |
|4. 成本管理			|/	|①编制成本管理计划<br>②成本估算<br>③成本预算|/ |④成本控制|/ | 
|5. 质量管理			|/  |①制定质量管理计划|②质量保证|③质量控制|/ |
|6. 人力资源管理	|/	|①制定人脸资源计划|②团队组建<br>③团队建设<br>④团队管理|/	|/	|
|7. 沟通管理			|/	|①沟通规划|/	|②沟通管理	|③沟通控制|/	|
|8. 干系人管理		|①干系人识别|②编制干系人管理计划|③管理干系人参与|④控制干系人参与|/	|
|9. 风险管理			|/	|①制定风险管理计划<br>②风险识别<br>③分析定性分析<br>④风险定量分析<br>⑤风险应对计划|/	|⑥风险监控|/	|
|10.采购管理		|/	|①编制采购管理计划|②实施采购|③控制采购|④结束采购|

## 二、评分标准
论文主要分为五方面进行评分：
* 切合题意（30%，22分左右）
* 引用深度与水准（20%，15分左右）
* 实践性（20%，15分左右）
* 表达能力（15%，11分左右）
* 综合能力与分析能力（15%，11分左右）



## 三、注意事项
1. 尽量不要写教育方面的系统，阅卷的都是教育方面的行家。
2. 注意内容不要全理论，要理论和实际经验相结合。
3. 针对题目来写，紧扣主题，问什么答什么。
3. 主要内容要真实，不要一看就知道是编的。
4. 不能用单机版、规模很小、没有明显特色的项目进行写作。
5. 注意严格控制篇幅，字不能太少也不能太多，2500字左右合适。
6. 写作思路要清晰，要有明显特色，注意错别字。
7. 项目已最近三年内的为佳，不能年代太久远了。
8. 已项目负责人的角度出发，条理清晰，首尾一致。
9. 练字！平均3秒不到就要写一个字，如果你日常没用笔，挑战还是蛮大的。
10. 即使书写错误也不要涂改。
11. 北京市必须写成XX市，及不能出现具体位置名称。
12. 

## 四、素材选着
* **建议选择**
	* 政府或大型ERP或信息系统项目。
	* 真正比较熟悉的项目。
	* 加分项目：国企、事业单位、医院、银行、大型企业相关的项目。
	* 加分内容：有应用最新热门技术的项目，如AI、区块链等。

* **不要选择**
	* 涉密&内部项目
	* 小微企业项目
	* 进销存系统
	* 图书管理系统
	* 单机版系统
	* 纯网站项目
	* 纯硬件项目
	* 纯技术项目

## 五、论文框架
论文写作时，主要分为
* **摘要**（300字，15分钟）
* **正文**（2000~2500字）
	* 项目背景`500字，20分钟`
	* 概述过度`150字，5分钟`
	* 项目描述`1300字`
	* 结尾总结`350字，15分钟`

**摘要**
* 背景、功能、技术、事情、结果、引出主题内容的结构。

**正文**
* 项目背景：背景、目标、框架、硬件。
* 概述过度：引出xx管理的工作尤为重要。
* 项目描述：
	* 分过程阐述。
	* 每段第一句话包含项目管理的过程。
	* 注意自己的角色。
	* 注意理论 联系实际。
	
* 结尾总结：
	* 结果、意义、缺陷。
	* 有负责人的霸气和担当，会继续学习和改进的谦虚。

阅卷老师不会有时间去详细的阅读一篇沦为，所以论文的开头、结尾以及中间内容的每个段落的第一句话尤为重要。

## 六、写作模板
### 1. 摘要
**万能模板**
`xxxx`年`xx`月，我作为`项目经理`参与了xx省xx市的`xx`项目的建设，该项目投资共`xx`万元人民币，建设工期为1年，通过该项目的建设，实现了该省市信息化的...`(根据实际项目进行补充，如建设意义、作用等)`，该项目于`xxxx`年`xx`月通过了业主方的验收，赢得了用户的好评。本文结合作者的实际经验，以该项目为例，讨论了信息系统项目建设过程中的`xx`管理`(具体根据考题来)`，主要从`xxxx（和前面的xx管理对应）`方面进行了论述。


**注意事项**
* 项目时间需在三年内。
* 担任角色都写项目经理。
* 不能出现具体的省市名称。
* 项目投资金额大于200万元，平常项目建议500万元，大型项目建议1000+万元。
* 建设工期1年左右。
* 字数在300字左右。


### 2. 项目背景
**万能模板**
`xxxx`年`xx`月，我作为项目经理参与了xx省xx市的`xx`项目的建设，该项目投资共`xx`万元人民币，建设工期为1年，通过该项目的建设，实现了该该省市信息化的...`(根据实际项目进行补充，如建设意义、作用等)`，该项目采用java语言开发，数据采用oracle 10g等技术...。

**注意事项**
* 是对摘要的细化描述及扩充。
* 英文单词不用按框填写，自然书写即可。
* 字数一般在500字左右。

### 3. 过度段落
**万能模板**
由于本项目的`xxxxxxx`重要意义，因此在本项目中`xx`管理尤为重要，我作为项目经理除了对其余管理英语进行恪尽职守的管理外，特别对`xx`管理领域从如下几个方面进行了管理。

**注意事项**
* 起到承上启下的作用。
* 字数在150字左右。

### 4. 项目描述
真正需要现场编写的内容。
需要认真学习十大管理。

**注意事项**
* 字数1300字左右。

### 5. 结尾段落
**万能模板**
经过我们团队的不懈努力，历时1年，本项目于`xxxx`年`xx`月，通过了业主方组织的验收，为用户...`解决了什么问题或达到了什么目的`，得到了业主的好评。本项目的成功得益于我成功的`xx`管理。当然，在本项目中，还有一些不足之处，比如：在项目的实施过程中，由于....。不过，经过我后期的纠偏或处理，并没有对项目产生什么影响。在后续的学习和工作中，我将不断的充电学习，和同行进行交流，提升自己的业务管理水平，力争为我国信息化建设做出自己的努力。

**注意事项**
* 一定要留时间结尾，不然可能不及格。
* 字数在350字左右。

## 七、历年题目分析
<table>
	<tr>
		<th width="40px">领域</th>
		<th>10<br>上</th>
		<th>10<br>下</th>
		<th>11<br>上</th>
		<th>11<br>下</th>
		<th>12<br>上</th>
		<th>12<br>下</th>
		<th>13<br>上</th>
		<th>13<br>下</th>
		<th>14<br>上</th>
		<th>14<br>下</th>
		<th>15<br>上</th>
		<th>15<br>下</th>
		<th>16<br>上</th>
		<th>16<br>下</th>
		<th>17<br>上</th>
		<th>17<br>下</th>
		<th>18<br>上</th>
		<th>18<br>下</th>
		<th>19<br>上</th>
		<th>19<br>下</th>
		<th>20<br>上</th>
		<th>20<br>下</th>
		<th>21<br>上</th>
	</tr>
	<tr>
		<th>整体</th>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>范围</th>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>进度</th>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>成本</th>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
	</tr>
	<tr>
		<th>质量</th>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>人力</th>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>沟通</th>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>风险</th>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>采购</th>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="background-color:#fcbd71">✔</td>
		<td></td>
	</tr>
	<tr>
		<th>干系人</th>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>

